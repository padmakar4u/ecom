from django.urls import path
from.import views 
from django.conf.urls import include 
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView
from.models import *
urlpatterns = [ 
    path('gettokend/', TokenObtainPairView.as_view(), name='gettoken'),
    path('refresh/', TokenRefreshView.as_view(), name='refresh'),
    path('verifytoken/', TokenVerifyView.as_view(), name='verifytoken'),
    path('register', views.registerV, name='register'),
] 
