from .models import *
from app.models import *
from rest_framework import serializers

class RegisterSerializer(serializers.ModelSerializer):
   class Meta:
      model = CustomUser
      fields = "__all__"
      extra_kwargs = {'password' : {'write_only': True}}
  
   def create(self, validated_data):
      user = CustomUser.objects.create_user(validated_data['email'], validated_data['password']
      )
      return user
