from django.shortcuts import render
from .serializers import *
from django.contrib.auth import login
import json
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from django.shortcuts import redirect
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import RegisterSerializer
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.utils.decorators import method_decorator
 
from django.http import HttpResponse, JsonResponse

@api_view(['GET', 'POST'])
def registerV(request):
       if request.method == 'POST':
           data=JSONParser().parse(request)
           serobj=RegisterSerializer(data=data)
           if serobj.is_valid():
                  serobj.save()
                  return JsonResponse(serobj.data)
           else:
              return JsonResponse(serobj.errors, status=400)
       else:
           return JsonResponse({"messsage":"method should be post"})