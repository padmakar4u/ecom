from .import views
from django.urls import path 
from django.conf.urls import include
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions


schema_view = get_schema_view(
   openapi.Info(
      title="e-commerce API",
      default_version='v1',
      description="by padmakar kasture",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="kasturepadmakar4u@gmail.com"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)



urlpatterns = [ 
   path('swagger', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
   path('', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
   path('products', views.getallp, name='getallp'),
   path('seller', views.sellerView1, name='seller'),
   path('seller/<int:id>', views.sellerV2, name='sellerv'),
   path('address', views.addaddress, name='address'),
   path('address/<int:id>', views.addresv2, name='addressv2'),
   path('rate/<int:id>', views.ratings, name='ratings'),
   path('cart', views.addcart, name='cart')

] 
