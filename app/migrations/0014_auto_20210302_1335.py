# Generated by Django 3.0.5 on 2021-03-02 08:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_rating_rd'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rating',
            name='rd',
            field=models.CharField(max_length=50),
        ),
    ]
