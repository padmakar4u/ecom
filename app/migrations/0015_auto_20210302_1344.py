# Generated by Django 3.0.5 on 2021-03-02 08:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0014_auto_20210302_1335'),
    ]

    operations = [
        migrations.RenameField(
            model_name='rating',
            old_name='rating',
            new_name='ratingv',
        ),
        migrations.AlterField(
            model_name='rating',
            name='createdAt',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
