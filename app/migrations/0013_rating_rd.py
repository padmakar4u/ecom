# Generated by Django 3.0.5 on 2021-03-02 08:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0012_auto_20210302_1209'),
    ]

    operations = [
        migrations.AddField(
            model_name='rating',
            name='rd',
            field=models.CharField(default='nfkn', max_length=50),
        ),
    ]
