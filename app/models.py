from django.db import models
from django.contrib.auth.models import User
from acounts.models import CustomUser
from django.conf import settings
# Create your models here.
class product(models.Model):
    pid=models.AutoField(primary_key=True, unique=True)
    pd=models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=25)
    description = models.TextField(max_length=70)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    warranty=models.BooleanField(default=False)
    returnp=models.BooleanField(default=False)
    # photo=models.ImageField()
    author=models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    quantity=models.IntegerField()
    created=models.DateTimeField(auto_now_add=True)
    updated=models.DateTimeField(auto_now=True)
    avrt=models.DecimalField(max_digits=4, decimal_places=2, default=0.0)
    


    def __str__(self):
        return self.name
    
class address(models.Model):
     aid=models.AutoField(primary_key=True, unique=True)
     mail=models.ForeignKey(CustomUser, blank=True, null=True, on_delete=models.CASCADE)   
     pincode=models.CharField(max_length=6)
     landmark=models.TextField(blank=True, null=True)
     address=models.TextField(blank=True, null=True)

     def __str__(self):
         return self.pincode    


class rating(models.Model):
    rd=models.CharField(max_length=50, unique=True)
    pid=models.ForeignKey(product , null=True , on_delete=models.CASCADE)
    empid=models.ForeignKey(CustomUser , null=True, on_delete=models.CASCADE)
    createdAt=models.DateTimeField(auto_now_add=True)
    ratingv=models.DecimalField(max_digits=3, decimal_places=1) 
    review=models.TextField(null=True, blank=True)
    
    def __str__(self):
        return self.rd

class pdetails(models.Model):
    pid=models.ForeignKey(product, blank=True, null=True, on_delete=models.CASCADE)
    height=models.DecimalField(null=True, max_digits=6, decimal_places=2)
    length=models.DecimalField(null=True, max_digits=6, decimal_places=2)
    width=models.DecimalField(null=True, max_digits=6, decimal_places=2)
    weight=models.DecimalField(null=True, max_digits=6, decimal_places=2)

    def __str__(self):
        return self.pid

    
class cart(models.Model):
    mail=models.ForeignKey(CustomUser, blank=True, on_delete=models.CASCADE)
    pid=models.ForeignKey(product, blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.mail
