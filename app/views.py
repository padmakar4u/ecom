from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import JSONParser
from django.http import HttpResponse, JsonResponse
import re
# Create your views here.
from app.models import *
from acounts.models import *
from .serializers import *
from django.db.models.aggregates import Avg


#view to list item
@api_view(['GET'])
# @permission_classes([IsAuthenticated])
# @authentication_classes([BasicAuthentication])
def getallp(request):
    # print(request.user.seller)
    obj=product.objects.all()
    serobj=getallser(obj, many=True)
    print(serobj.data)
    return Response(serobj.data, status=200)


    
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
@api_view(['GET', 'POST'])
def sellerView1(request):
  if request.method=='GET':
    obj=product.objects.filter(author=request.user.email)
    serobj=sellerSerGet(obj, many=True)
    return Response(serobj.data, status=200)
  if request.method=='POST':
    if request.user.seller== True:
       data=JSONParser().parse(request)
       pname=data['name']
       author=request.user.email
       pd=pname+author
       res = re.sub(' +', '', pd) 
       pd=res.lower()
       print(pd)
       context={'author':author, 'pd':pd}
       data.update(context)
       serializer=sellerSerPost(data=data)
       if serializer.is_valid():
           serializer.save()
           return JsonResponse(serializer.data, status=201)
       else:
         return JsonResponse(serializer.errors, status=400)
    else:
       return Response({'messasge':'you are not a seller'})



@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
@api_view(['GET', 'PUT', 'DELETE'])
def sellerV2(request, id):
    if request.method =='GET':
        a=request.user.email
        obj=product.objects.filter(pid=id, author=a)
        if not obj:
            return Response({"message":"it is not your product"}) 
        else:
          serobj=sellerSerGet(obj, many=True)
          return Response(serobj.data, status=200)
   
   
    if request.method =='PUT':
          objp=product.objects.get(pid=id)
          serobj=sellerput(objp, data=request.data)
          if serobj.is_valid():
              serobj.save()
              return Response(serobj.data, status=200)
          else:
              return Response(serobj.errors, status=400)
    
    if request.method=="DELETE":
         objd=product.objects.get(pid=id)
         objd.delete()
         return Response({'message': 'item deleted'} ,status=204)


@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
@api_view(['GET', 'POST'])  
def addaddress(request):
      if request.method=='GET':
          obj=address.objects.filter(mail=request.user.email)
          serobj=addressSer(obj, many=True)
          return Response(serobj.data, status=200) 
    
      elif request.method=='POST':
          data=JSONParser().parse(request)
          author=request.user.email
          context={'mail':author}
          data.update(context)
          serobj=addressSer(data=data)
          if serobj.is_valid():
                serobj.save()
                return JsonResponse(serobj.data, status=201)
          else:
              return JsonResponse(serobj.errors, status=400)
 
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
@api_view(['GET','PUT', 'DELETE'])       
def addresv2(request, id):
   if request.method =='GET':
       a=request.user.email
       print(a)
       obj=address.objects.get(aid=id)
       print(obj)
       if obj:
           serobj=addressPut(obj, many=False)
           return Response(serobj.data, status=200)
       else:
           print("loop")
           return Response({"message":" address not present"}, status=200)   
       
           
   elif request.method=='PUT':
       a=request.user.email
       print(a)
       obj=address.objects.filter(aid=id, mail=a)
       if obj:
           print("in loop")
           serobj=addressPut(obj, data=request.data)
           if serobj.is_valid():
               serobj.save()
               return Response(serobj.data, status=200)
           else:
               return Response(serobj.errors, status=400)
       else:
           return Response({"message":" address with this id and email doesn't exist"})
   elif request.method=='DELETE':
       a=request.user.email
       obj=address.objects.get(aid=id, mail=a)
       if obj:
           obj.delete()
           return Response({"message":"deleted"}, status=200)
       else:
           return Response({"message":"no content"}, status=204)



@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
@api_view(['GET', 'POST', 'PUT', 'DELETE'])  
def ratings(request, id):
      if request.method=='GET':
          articleobj=rating.objects.filter(pid=id)
          if not articleobj:
               newobj=product.objects.filter(pid=id)
               if not newobj:
                   return Response({"message":"there is no such product"})
               else:
                   return Response({"message":"no ratings yet"})
          else:
            serobj=ratingSer(articleobj, many=True)
            return Response(serobj.data, status=200)
    
      elif request.method=='POST':
        obj1=product.objects.filter(pid=id)
        if not obj1:
            return Response({"message":"there is no such product"})
        else:  
          data=JSONParser().parse(request)
          author=request.user.email
          rd=str(id)+author
          res = re.sub(' +', '', rd) 
          pd=res.lower()
          context={'empid':author, 'pid':id , 'rd':pd}
          data.update(context)
          serobj=ratingSer(data=data)
          if serobj.is_valid():
                serobj.save()
                obj2=rating.objects.filter(pid=id).aggregate(Avg('ratingv'))
                avrt=round(obj2['ratingv__avg'], 1)
                obj3=product.objects.filter(pid=id).update(avrt=avrt)
                
                return JsonResponse(serobj.data, status=201)
          return JsonResponse(serobj.errors, status=400)
      if request.method =='PUT':
          a=request.user.email
          obj=rating.objects.get(pid=id, empid=a)
          if obj:
              serobj=ratingSer(obj, data=request.data)
              if serobj.is_valid():
                serobj.save()
              return Response(serobj.data, status=200)
          else:
              return Response(serobj.errors, status=400)
     
      if request.method=="DELETE":
         a=request.user.email
         obj=rating.objects.get(pid=id, empid=a)
         obj.delete()
         return Response({'message': 'item deleted'} ,status=204)


        



       
# add to cart

@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
@api_view(['GET', 'POST'])
def addcart(request):
    if request.method=='GET':
        obj=cart.objects.filter(mail=request.user.email)
        serobj=cartser(obj, many=True)
        return Response(serobj.data, status=200)
    if request.method=='POST':
        data=JSONParser().parse(request)
        context={'mail':request.user.email}
        data.update(context)
        serobj=cartser(data=data)
        if serobj.is_valid():
            serobj.save()
            return Response(serobj.data, status=201)
        return JsonResponse(serobj.errors, status=400)

    