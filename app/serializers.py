from .models import *
from acounts.models import *
from rest_framework import serializers

# serializer to list all items
class getallser(serializers.ModelSerializer):
    class Meta:
        model =product
        Fields="__all__"
        exclude = ['author']


# serializer for seller
class sellerSerPost(serializers.ModelSerializer):
    class Meta:
        model = product
        Fields=['name', 'description', 'price', 'warranty', 'returnp', 'quantity', 'pd']
        exclude = []

class sellerSerGet(serializers.ModelSerializer):
    class Meta:
        model = product
        Fields="__all__"
        exclude=[]

class sellerput(serializers.ModelSerializer):
    class Meta:
        model = product
        fields=['name', 'description', 'price', 'warranty', 'returnp', 'quantity']
        exclude = []

#add address

class addressSer(serializers.ModelSerializer):
    class Meta:
        model =address
        fields="__all__"
        exclude = []

class ratingSer(serializers.ModelSerializer):
    class Meta:
        model=rating
        fields="__all__"
        exclude=[]

class cartser(serializers.ModelSerializer):
    class Meta:
        model=cart
        fields="__all__"
        exclude=[]

class pdetailsSer(serializers.ModelSerializer):
    class Meta:
        model=pdetails
        fields="__all__"
        exclude=[]
class addressPut(serializers.ModelSerializer):
  class Meta:
      model=address
      fields="__all__"
      exclude=[]