from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(product)
admin.site.register(address)
admin.site.register(rating)
admin.site.register(pdetails)
admin.site.register(cart)