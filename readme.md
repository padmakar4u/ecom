    Basic ecommerce api 
                  -  Django Rest Framework

Overview
        Basic ecommerce api designed in django rest framework with three types of user i)customer ii)seller iii)superuser

Role of customer-
         Customer can create,update,delete addresses,cart,ratings
Role of seller-
        Seller can add,update delete products 
        Seller is also a customer
Role of superuser-
         Superuser can register seller
         Superuser is also customer and may be seller
Endpoints
 /products
Can view all available products
     Methods allowed- GET
     Permissions-any one
     Authentication- no required
/seller
Can view and add own products to sell
      Method allowed-GET,POST
      Permissions- only seller
      Authentication- basic authentication
      Json
Method post ex-   

{
       "name": "product name",
        "description": "product description",
        "price": "45.50",
        "warranty": true,
        "returnp": true,
        "quantity": 25
}


/seller/{product_id}
  Can view,update,delete
        Method allowed-;GET,PUT,DELETE
        Permissions- only seller
        authentication - basic 
       json
  Method put ex-
{
       "name": "product name",
        "description": "product description",
        "price": "45.50",
        "warranty": true,
        "returnp": true,
        "quantity": 25
}


/address
    Can view and add address
          Method allowed - GET,POST
          Permission- customers
          Authentication - basic
         json
   Method post ex-
     
{
    "pincode":"431517",
    "address":"talni",
    "landmark":"hanuman temple talni"
}

/address/{address_id}
    Can view,update and delete address
            Method allowed-’GET,PUT,DELETE
            Permission- customer
            Authentication-basic authentication
           Json
   Method put ex-{
    "pincode":"431517",
    "address":"talni",
    "landmark":"hanuman temple talni"
}   
/rate/{product_id}
    Can add,update,delete rating
          Method allowed- GET,POST,PUT,DELETE
          Permission - customer
          Authentication - basic
         Json
   Method post/put ex-
   {
"ratingv":"5.0",
"review":"good product"
}

            
  
/cart
   Can  add to cart and enlist cart
        Method allowed -GET,POST
       Permission- customer
       Authentication- Basic
      Json
Method post ex
{
    "pid":8
} 


/acount/register
    Can register new customer
          Method -POST
      Permission- None
      Authentication - none
     Json
Method post ex
{
    "email":"user2@gmail.com",
    "password":"Password@123"  
}

         

User list
         
username                             password     Is seller      Is super user
kasturepadmakar4u@gmail.com          12345678      true           true
user@gmail.com                      Password@123    false          false
user1@gmail.com                     Password@123    true           false




